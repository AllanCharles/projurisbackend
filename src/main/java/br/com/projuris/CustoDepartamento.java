/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.projuris;

import java.math.BigDecimal;

/**
 *
 * @author Charles
 */
public class CustoDepartamento {

    private String departamento;
    private BigDecimal custo;
// getters e setters

    public CustoDepartamento(String departamento, BigDecimal custo) {
        this.departamento = departamento;
        this.custo = custo;
    }
    
    

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public BigDecimal getCusto() {
        return custo;
    }

    public void setCusto(BigDecimal custo) {
        this.custo = custo;
    }

}
