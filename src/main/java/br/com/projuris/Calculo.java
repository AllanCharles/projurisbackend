/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.projuris;


import java.util.List; 
/**
 *
 * @author Charles
 */
public interface Calculo {

    public List<CustoCargo> custoPorCargo(List<Funcionario> funcionarios);

    public List<CustoDepartamento> custoPorDepartamento(List<Funcionario> funcionarios);
}
