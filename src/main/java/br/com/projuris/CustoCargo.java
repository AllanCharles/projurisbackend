/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.projuris;

import br.com.projuris.exception.DAOException;
import java.math.BigDecimal;

/**
 *
 * @author Charles
 */
public class CustoCargo {

    private String cargo;
    private BigDecimal custo;
// getters e setters;

    public CustoCargo(String cargo, BigDecimal custo) {
        this.cargo = cargo;
        this.custo = custo;
    }
    

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public BigDecimal getCusto() {
        return custo;
    }

    public void setCusto(BigDecimal custo) {
        this.custo = custo;
    }

}
