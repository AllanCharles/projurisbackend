/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.projuris;

import br.com.projuris.exception.DAOException;
import br.com.projuris.util.Moeda;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author Charles
 */
public class MyCalculo implements Calculo {

    @Override
    public List<CustoCargo> custoPorCargo(List<Funcionario> arg0) {
        List<CustoCargo> LISTA_CUSTO_CARGO = new LinkedList<>();
        CustoCargo CUSTO_CARGO = null;
        arg0.sort(Comparator.comparing(Funcionario::getCargo));

        for (Funcionario f : arg0) {
            if (CUSTO_CARGO == null) {
                CUSTO_CARGO = new CustoCargo(f.getCargo(), BigDecimal.ZERO);
                LISTA_CUSTO_CARGO.add(CUSTO_CARGO);
            }

            if (f.getCargo().equals(CUSTO_CARGO.getCargo())) {
                for (CustoCargo custoC : LISTA_CUSTO_CARGO) {
                    if (f.getCargo().equals(custoC.getCargo())) {
                        custoC.setCusto(custoC.getCusto().add(f.getSalario()));
                    }
                }

            } else {
                CUSTO_CARGO = new CustoCargo(f.getCargo(), f.getSalario());
                LISTA_CUSTO_CARGO.add(CUSTO_CARGO);
            }
        }

        return LISTA_CUSTO_CARGO;
    }

    @Override
    public List<CustoDepartamento> custoPorDepartamento(List<Funcionario> arg0) {
        List<CustoDepartamento> LISTA_CUSTO_DEPARTAMENTO = new LinkedList<>();
        CustoDepartamento CUSTO_DEPARTAMENTO = null;
        arg0.sort(Comparator.comparing(Funcionario::getDepartamento));

        for (Funcionario f : arg0) {
            if (CUSTO_DEPARTAMENTO == null) {
                CUSTO_DEPARTAMENTO = new CustoDepartamento(f.getDepartamento(), BigDecimal.ZERO);
                LISTA_CUSTO_DEPARTAMENTO.add(CUSTO_DEPARTAMENTO);
            }

            if (f.getDepartamento().equals(CUSTO_DEPARTAMENTO.getDepartamento())) {
                for (CustoDepartamento custoD : LISTA_CUSTO_DEPARTAMENTO) {
                    if (f.getDepartamento().equals(custoD.getDepartamento())) {
                        custoD.setCusto(custoD.getCusto().add(f.getSalario()));
                    }
                }

            } else {
                CUSTO_DEPARTAMENTO = new CustoDepartamento(f.getDepartamento(), f.getSalario());
                LISTA_CUSTO_DEPARTAMENTO.add(CUSTO_DEPARTAMENTO);
            }
        }

        return LISTA_CUSTO_DEPARTAMENTO;
    }

    public static void main(String[] args) {

        Funcionario funcionario1 = new Funcionario("Assistente", "Administrativo", new BigDecimal(1000.0));
        Funcionario funcionario2 = new Funcionario("Gerente", "Administrativo", new BigDecimal(7000.70));
        Funcionario funcionario3 = new Funcionario("Diretor", "Administrativo", new BigDecimal(10000.45));
        Funcionario funcionario4 = new Funcionario("Assistente", "Financeiro", new BigDecimal(1300.9));
        Funcionario funcionario5 = new Funcionario("Gerente", "Financeiro", new BigDecimal(7500));
        Funcionario funcionario6 = new Funcionario("Diretor", "Financeiro", new BigDecimal(11000.0));
        Funcionario funcionario7 = new Funcionario("Estagiário", "Jurídico", new BigDecimal(700.4));
        Funcionario funcionario8 = new Funcionario("Assistente", "Jurídico", new BigDecimal(1800.90));
        Funcionario funcionario9 = new Funcionario("Gerente", "Jurídico", new BigDecimal(9500.50));
        Funcionario funcionario10 = new Funcionario("Diretor", "Jurídico", new BigDecimal(13000.0));
        List<Funcionario> listaFuncionario = new ArrayList<>();
        listaFuncionario.add(funcionario1);
        listaFuncionario.add(funcionario2);
        listaFuncionario.add(funcionario3);
        listaFuncionario.add(funcionario4);
        listaFuncionario.add(funcionario5);
        listaFuncionario.add(funcionario6);
        listaFuncionario.add(funcionario7);
        listaFuncionario.add(funcionario8);
        listaFuncionario.add(funcionario9);
        listaFuncionario.add(funcionario10);

        System.out.println("----------------------------------CUSTO POR CARGO");
        MyCalculo myCalculo = new MyCalculo();
        for (CustoCargo custoCargo : myCalculo.custoPorCargo(listaFuncionario)) {
            System.out.println(custoCargo.getCargo());
            System.out.println(Moeda.mascaraDinheiro(custoCargo.getCusto(), Moeda.DINHEIRO_REAL));
        }

        System.out.println("----------------------------------CUSTO POR DEPARTAMENTO");
        for (CustoDepartamento custoDepartamento : myCalculo.custoPorDepartamento(listaFuncionario)) {
            System.out.println(custoDepartamento.getDepartamento());
            System.out.println(Moeda.mascaraDinheiro(custoDepartamento.getCusto(), Moeda.DINHEIRO_REAL));
        }

    }

}
