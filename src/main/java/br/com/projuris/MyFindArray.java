/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.projuris;

import java.util.Arrays;

/**
 *
 * @author Charles
 */
public class MyFindArray implements FindArray {

    @Override
    public int findArray(int[] arg0, int[] arg1) {
        int POSICAO_ARG0 = -1;
        try {
            for (int int_arg_1 = 0; int_arg_1 < arg1.length; int_arg_1++) {
                for (int int_arg_0 = 0; int_arg_0 < arg0.length; int_arg_0++) {
                    if (arg1[int_arg_1] == arg0[int_arg_0]) {
                        POSICAO_ARG0 = int_arg_0;
                    }
                }
                if (POSICAO_ARG0 != -1) {
                    return POSICAO_ARG0;
                }
            }
        } catch (Exception erro1) {
            System.out.println("Tratar Exception");
        }
        return POSICAO_ARG0;

    }

    public static void main(String[] args) {
        MyFindArray myFindArray = new MyFindArray();

        int[] array_01 = {4, 9, 3, 7, 8};
        int[] array_02 = {3, 7};
        System.out.println(myFindArray.findArray(array_01, array_02));
        
//        int[] array_01 = {1,3,5};
//        int[] array_02 = {1};
//        System.out.println(myFindArray.findArray(array_01, array_02));
//
        // No exemplo estava [7,8,9] e [8,9,10] deve retornar -1 nessa condição do exemplo não retornara -1 jamais
//        int[] array_01 = {7, 8, 9};
//        int[] array_02 = {10, 11, 12};
//        System.out.println(myFindArray.findArray(array_01, array_02));
//
//        int[] array_01 = {4, 9, 3, 7, 8, 3, 7, 1};
//        int[] array_02 = {3, 7};
//        System.out.println(myFindArray.findArray(array_01, array_02));
    }

}
